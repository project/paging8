Paging 8

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Filter plugin to make it possible to paginate WYSIWYG fields using
<!--pagebreak--> tags.

* For a full description of the module, visit the project page:
   https://www.drupal.org/project/admin_menu


REQUIREMENTS
------------

Nothing specific. Can be used with clean drupal installation.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

You need to have access to edit text editors.
If you don't have these permissions you need to configure the user permissions
in Administration » People » Permissions.

Add Paging 8 filter to a text editor, configure it at the bottom of text editor
editing page (specify: do you want to show top pager, bottom pager and title
list, do you want to use default CSS styles). Then go to content, add tags as
specified in descriptions on the configuration form to the field that uses the
text editor you've just edited. After that you can go to the content view page
and see results.


MAINTAINERS
-----------

Current maintainers:
 * Eugene Chalenko (tyzzzon) - https://www.drupal.org/user/3365318
